public enum Figura {
    AS("As",11),
    KROL("Król",4),
    DAMA("Dama",3),
    WALET("Walet",2),
    DZIESIATKA("Dziesiatka",10),
    DZIEWIATKA("Dziewiatka",9),
    OSEMKA("Osemka",8),
    SIODEMKA("Siodemka",7),
    SZOSTKA("Szostka",6),
    PIĄTKA("Piątka",5),
    CZWÓRKA("Czwórka",4),
    TRÓJKA("Trójka",3),
    DWÓJKA("Dwójka",2);

    String nazwa;
    int wartosc;

    Figura(String nazwa, int wartosc) {
        this.nazwa = nazwa;
        this.wartosc = wartosc;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getWartosc() {
        return wartosc;
    }
}
