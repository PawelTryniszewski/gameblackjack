import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EmptyStackException;
import java.util.InputMismatchException;

public class JMenu extends JFrame implements ActionListener, ChangeListener {
    public static void main(String[] args) {
        JMenu appMenu = new JMenu();
        appMenu.setDefaultCloseOperation(EXIT_ON_CLOSE);
        appMenu.setVisible(true);
    }

    private Konta KontoGracza = new Konta(000011110000111, 1000);
    private Konta KontoKasyna = new Konta(000022220000222, 1000000);
    private int sumaPunktow = 0;
    private int obstawianaKwota = 0;
    private int wygraneGracza = 0;
    private int wygraneKasyna = 0;
    private Krupier krupier = new Krupier();


    private JButton bPotasuj, bCzekaj, bDobierz, bPostaw, bKoniec, bPrzelejNaKonto;
    private JSlider sWplac;
    private JTextField textFieldPostaw, textFieldStanKonta, textFieldWplac;

    private JMenu() {
        setTitle("Black Jack");
        setSize(600, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);

        JLabel stanKonta = new JLabel("Stan Konta");
        stanKonta.setBounds(350, 50, 70, 20);
        add(stanKonta);


        textFieldPostaw = new JTextField("");
        textFieldPostaw.setBounds(200, 50, 100, 20);
        add(textFieldPostaw);
        textFieldPostaw.addActionListener(this);
        textFieldPostaw.setToolTipText("Postaw Kwote");// po najechaniu myszką na pole wyświetla się podpowiedź.

        textFieldStanKonta = new JTextField(String.valueOf(KontoGracza.getStanKonta()));
        textFieldStanKonta.setBounds(450, 50, 70, 20);
        textFieldStanKonta.setFont(new Font("SanSerif", Font.BOLD, 12));
        add(textFieldStanKonta);
        textFieldStanKonta.setToolTipText("Stan Konta");

        textFieldWplac = new JTextField();
        textFieldWplac.setBounds(200, 300, 100, 20);
        add(textFieldWplac);
        textFieldWplac.setToolTipText("Wpłać kwotę");

        sWplac = new JSlider(0, 10000, 0);
        sWplac.setBounds(50, 350, 500, 40);
        sWplac.setMajorTickSpacing(1000);
        sWplac.setPaintLabels(true);//robi etykiety co 20 widoczne
        sWplac.setPaintTicks(false);// przedziałka co 5 staje sie widoczna
        sWplac.addChangeListener(this);
        add(sWplac);

        bPotasuj = new JButton("Potasuj");
        bPotasuj.setBounds(50, 200, 100, 25);
        bPotasuj.addActionListener(this);
        bPotasuj.setMnemonic('p');
        add(bPotasuj);

        bCzekaj = new JButton("Czekaj");
        bCzekaj.setBounds(50, 100, 100, 25);
        bCzekaj.addActionListener(this);
        bCzekaj.setMnemonic('c');
        add(bCzekaj);

        bDobierz = new JButton("Dobierz");
        bDobierz.setBounds(50, 150, 100, 25);
        bDobierz.addActionListener(this);
        bDobierz.setMnemonic('d');
        add(bDobierz);

        bPostaw = new JButton("Postaw");
        bPostaw.setBounds(50, 50, 100, 25);
        bPostaw.addActionListener(this);
        add(bPostaw);

        bKoniec = new JButton("Koniec");
        bKoniec.setBounds(50, 250, 100, 25);
        bKoniec.addActionListener(this);
        bKoniec.setMnemonic('k');
        add(bKoniec);

        bPrzelejNaKonto = new JButton("Wpłać $");
        bPrzelejNaKonto.setBounds(50, 300, 100, 25);
        bPrzelejNaKonto.addActionListener(this);
        bPrzelejNaKonto.setMnemonic('o');
        add(bPrzelejNaKonto);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object z = e.getSource();
        if (z == (bPotasuj)) {
            Talia.getInstance().potasuj();
        } else if (z == bDobierz) {
            try {
                if (sumaPunktow == 0) {
                    sumaPunktow += Talia.getInstance().dobierzKarte();
                }
                try {
                    sumaPunktow += Talia.getInstance().dobierzKarte();
                    JOptionPane.showMessageDialog(null, "Twoja suma punktów: " + sumaPunktow);
                } catch (EmptyStackException b) {
                    JOptionPane.showMessageDialog(null, "Koniec kart, potasuj nowa talię");

                }
                if (sumaPunktow > 21) {
                    JOptionPane.showMessageDialog(null, "Przegrałeś");
                    wygraneKasyna++;
                    if (obstawianaKwota > 0) {
                        textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));
                        obstawianaKwota = 0;
                    }
                    sumaPunktow = 0;
                }
                if (sumaPunktow == 21) {
                    JOptionPane.showMessageDialog(null, "Wygrałeś!");
                    wygraneGracza++;
                    if (obstawianaKwota > 0) {
                        KontoGracza.wplacSrodki(KontoKasyna.pobierzSrodki(obstawianaKwota * 2));
                        textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));
                        obstawianaKwota = 0;
                    }
                    sumaPunktow = 0;

                }
            } catch (EmptyStackException b) {
                JOptionPane.showMessageDialog(null, "Potasuj karty.");
            }
        } else if (z == textFieldPostaw || z == bPostaw) {
            if (sumaPunktow == 0 && obstawianaKwota == 0) {
                try {
                    obstawianaKwota = Integer.parseInt(textFieldPostaw.getText());
                    if (KontoGracza.stanKonta / 2 >= obstawianaKwota) {
                        KontoKasyna.wplacSrodki(KontoGracza.pobierzSrodki(obstawianaKwota));
                    } else if (KontoGracza.stanKonta / 2 <= obstawianaKwota) {
                        JOptionPane.showMessageDialog(null, "Nie mozesz postawic wiecej niz polowa Twojego stanu konta.");
                        obstawianaKwota = 0;
                    }
                } catch (InputMismatchException | NumberFormatException exception) {
                    JOptionPane.showMessageDialog(null, "Zła wartość.");
                }
            }
            if (obstawianaKwota > 0) {
                JOptionPane.showMessageDialog(null, "Postawiłeś " + String.valueOf(obstawianaKwota));
            }
        } else if (z == bKoniec) {
            int odp = JOptionPane.showConfirmDialog(null, "Wygrałeś " + wygraneGracza +
                            " gier z " + (wygraneGracza + wygraneKasyna), "Czy chcesz wyjść?",
                    JOptionPane.YES_NO_OPTION);
            if (odp == JOptionPane.YES_OPTION) {
                dispose();
            } else if (odp == JOptionPane.NO_OPTION) {
                JOptionPane.showMessageDialog(null, "Wiedziałem..");
            } else if (odp == JOptionPane.CLOSED_OPTION)
                JOptionPane.showMessageDialog(null, "Czyli jednak nie wychodzimy durniu?",
                        "Tytuł", JOptionPane.WARNING_MESSAGE);
            wygraneGracza = 0;
            wygraneKasyna = 0;
        } else if (z == bCzekaj) try {
            int wynikKrupiera;
            wynikKrupiera = krupier.graj(Talia.getInstance());
            if (wynikKrupiera == 21) {
                JOptionPane.showMessageDialog(null, "Przegrałeś");
                wygraneKasyna++;
                if (obstawianaKwota > 0) {
                    textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));
                    obstawianaKwota = 0;
                }
                wynikKrupiera = 0;
                sumaPunktow = 0;
            } else if (wynikKrupiera > 21) {
                JOptionPane.showMessageDialog(null, "Wygrałeś!");
                wygraneGracza++;
                if (obstawianaKwota > 0) {
                    KontoGracza.wplacSrodki(KontoKasyna.pobierzSrodki(obstawianaKwota * 2));
                    textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));
                    obstawianaKwota = 0;
                }
                wynikKrupiera = 0;
                sumaPunktow = 0;
            } else if (wynikKrupiera < 21) {
                if (wynikKrupiera <= sumaPunktow) {
                    JOptionPane.showMessageDialog(null, "Wygrałeś!");
                    wygraneGracza++;
                    if (obstawianaKwota > 0) {
                        KontoGracza.wplacSrodki(KontoKasyna.pobierzSrodki(obstawianaKwota * 2));
                        textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));
                        obstawianaKwota = 0;
                    }
                    wynikKrupiera = 0;
                    sumaPunktow = 0;

                } else if (wynikKrupiera > sumaPunktow) {
                    JOptionPane.showMessageDialog(null, "Przegrałeś");
                    wygraneKasyna++;
                    if (obstawianaKwota > 0) {
                        textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));
                        obstawianaKwota = 0;
                    }
                    wynikKrupiera = 0;
                    sumaPunktow = 0;
                }
            }
        } catch (EmptyStackException b) {
            JOptionPane.showMessageDialog(null, "Potasuj karty");
        }
        else if (z == bPrzelejNaKonto) {
            try {
                int wplacanaKwota = Integer.parseInt(textFieldWplac.getText());
                KontoGracza.wplacSrodki(wplacanaKwota);
                textFieldStanKonta.setText(String.valueOf(KontoGracza.getStanKonta()));

            }catch (InputMismatchException | NumberFormatException exception){
                JOptionPane.showMessageDialog(null, "Zła wartość.");
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        textFieldWplac.setText(String.valueOf(sWplac.getValue()));

    }
}

